import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import Home from './routes/home.js';
import Store from './routes/store.js';


const app= document.getElementById('app');



ReactDOM.render(<Router>
                    <div>
                        <Route exact path="/" component={Home}/>
                        <Route path="/Store" component={Store}/>
                    </div>
                </Router>,app);
