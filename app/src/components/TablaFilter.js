import React from 'react';
import SearchBar from './SearchBar';
import Tabla from './Tabla';
export default class TablaFilter extends React.Component{
    constructor(){
        super();
        this.nombre="tabla filter arturo rojas!";
        this.state={
            filter:null
        }
    }
    filterList(ev){
        let filter= ev.target.value
        this.setState({
          filter:filter  
        })
    }
	render(){
		return (
            <div>
             <h1>{this.nombre}</h1>
			<SearchBar onChange={this.filterList.bind(this)}/>
            <Tabla source={this.props.source} filter={this.state.filter}/>
            </div>
        );
	}
}