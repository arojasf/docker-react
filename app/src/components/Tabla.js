import React from 'react';
import Row from './Row';
import CategoryRow from './CategoryRow';

export default class Tabla extends React.Component{
    constructor(){
        super();
        this.nombre="Tabla Principal";
    }
	render(){
        let rows=[];
        let last_category=null;
        //console.log(this.props.source);
        if(this.props.filter!=null && this.props.source!=null){
            let filter = this.props.filter;
            console.log(filter);
            this.props.source.forEach((item)=>{
                if(item.name.indexOf(filter)>-1){
                    if(item.category!=last_category){
                        rows.push(<CategoryRow category={item.category} key={item.category}/> );
                    }
                    rows.push(<Row item={item} key={item.name}/>)              
                    last_category= item.category;
                }
            })
        }
        else if(this.props.source!=null){
            this.props.source.forEach((item)=>{
                if(item.category!=last_category){
                    rows.push(<CategoryRow category={item.category} key={item.category}/> );
                }
                rows.push(<Row item={item} key={item.name}/>)              
                last_category= item.category;
            })
        }
        else{
            rows.push(<h1>Cargando</h1>);
        }
		return (
			<div>
                <h1>{this.nombre}</h1>
                {rows}
            </div>
        );
	}
}