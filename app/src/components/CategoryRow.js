import React from 'react';

export default class CategoryRow extends React.Component{
    constructor(){
        super();
    }
	render(){
		return (
			<div>
                <h1>{this.props.category}</h1>
            </div>
        );
	}
}